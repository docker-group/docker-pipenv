FROM python:3.7

COPY ./rc.local /etc/rc.local
RUN chmod +x /etc/rc.local
COPY . /bd_build

RUN /bd_build/prepare.sh && \
	/bd_build/system_services.sh && \
	/bd_build/utilities.sh && \
	/bd_build/cleanup.sh

ENV DEBIAN_FRONTEND="teletype" \
    LANG="en_US.UTF-8" \
    LANGUAGE="en_US:en" \
    LC_ALL="en_US.UTF-8"

RUN pip install pip --upgrade && \
    mkdir /var/www/ && mkdir -p /etc/service/project && \
    apt-get update && \
    apt-get install -y libxml2 git gdal-bin nano less wait-for-it gettext && \
    pip uninstall boto3 && pip install awscli pipenv --upgrade && pip install botocore --upgrade
ADD run /etc/service/project/
RUN chmod +x /etc/service/project/run

# Make docker download certs on startup from s3
RUN if [ ! -f /etc/rc.local ]; then echo "#!/bin/bash -e" > /etc/rc.local && echo "#Added by dockerfile" >> /etc/rc.local && echo "exit 0" >> /etc/rc.local; fi
RUN sed -i "\$i if test \"\$ENABLE_S3\" == \"true\"; then aws s3 sync s3://frankfurt-$env-$project_name/certs/ /etc/ssl/certs/; else echo done; fi" /etc/rc.local

RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

CMD ["/sbin/my_init"]
